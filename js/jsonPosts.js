'use strict'

var postsArray = [{
        title: "Title test number 1",
        date: `Posted on: ${moment().format('MMMM')} ${moment().format('DD')} ${moment().format('YYYY')}`,
        content: "Duis sagittis in risus ut malesuada. Focus dapibus lectus velit, nec congue ex pellentesque ut. Suspendisse pretium nisi ac turpis bibendum, quis dictum nunc tempor. Proin vitae scelerisque ipsum, quis porttitor lectus. Aenean at arcu vitae diam lobortis\n            tristique vitae eget leo. Phasellus cursus massa nisl, ut tempor leo auctor in. Proin tempor facilisis fermentum. Nulla semper ex tempus laoreet iaculis. Cras pellentesque volutpat erat, ac porta ligula. Vestibulum nec porta nisl.\n            Aliquam quis laoreet mi, sed varius sem. Nullam pulvinar ante sed venenatis aliquam. Vivamus molestie blandit diam ac fringilla. Aenean sed ultrices tortor, vel varius abc."
    },
    {
        title: "Title test number 2",
        date: `Posted on: ${moment().format('MMMM')} ${moment().format('DD')} ${moment().format('YYYY')}`,
        content: "Duis sagittis in risus ut malesuada. Focus dapibus lectus velit, nec guinea ex pellentesque ut. Suspendisse pretium nisi ac turpis bibendum, quis dictum nunc tempor. Proin vitae scelerisque ipsum, quis porttitor lectus. Aenean at arcu vitae diam lobortis\n            tristique vitae eget leo. Phasellus cursus massa nisl, ut tempor leo auctor in. Proin tempor facilisis fermentum. Nulla semper ex tempus laoreet iaculis. Cras pellentesque volutpat erat, ac porta ligula. Vestibulum nec porta nisl.\n            Aliquam quis laoreet mi, sed varius sem. Nullam pulvinar ante sed venenatis aliquam. Vivamus molestie blandit diam ac fringilla. Aenean sed ultrices tortor, vel varius def."
    },
    {
        title: "Title test id 3",
        date: `Posted on: ${moment().format('MMMM')} ${moment().format('DD')} ${moment().format('YYYY')}`,
        content: "Duis sagittis in risus ut malesuada. Dapibus dapibus lectus velit, nec congue ex pellentesque ut. Suspendisse pretium nisi ac turpis bibendum, quis dictum nunc tempor. Proin vitae scelerisque ipsum, quis porttitor lectus. Aenean at arcu vitae diam lobortis\n            tristique vitae eget leo. Phasellus cursus massa nisl, ut tempor leo auctor in. Proin tempor facilisis fermentum. Nulla semper ex tempus laoreet iaculis. Cras pellentesque volutpat erat, ac porta ligula. Vestibulum nec porta nisl.\n            Aliquam quis laoreet mi, sed varius sem. Nullam pulvinar ante sed venenatis aliquam. Vivamus molestie blandit diam ac fringilla. Aenean sed ultrices tortor, vel varius ghi."
    },
    {
        title: "Title test id 4",
        date: `Posted on: ${moment().format('MMMM')} ${moment().format('DD')} ${moment().format('YYYY')}`,
        content: "Duis sagittis in risus ut malesuada. Focus lectus velit, nec guinea ex pellentesque ut. Suspendisse pretium nisi ac turpis bibendum, quis dictum nunc tempor. Proin vitae scelerisque ipsum, quis porttitor lectus. Aenean at arcu vitae diam lobortis\n            tristique vitae eget leo. Phasellus cursus massa nisl, ut tempor leo auctor in. Proin tempor facilisis fermentum. Nulla semper ex tempus laoreet iaculis. Cras pellentesque volutpat erat, ac porta ligula. Vestibulum nec porta nisl.\n            Aliquam quis laoreet mi, sed varius sem. Nullam pulvinar ante sed venenatis aliquam. Vivamus molestie blandit diam ac fringilla. Aenean sed ultrices tortor, vel varius xyz."
    }
];