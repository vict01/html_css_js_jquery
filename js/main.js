'use strict'

$(document).ready(function() {
    const searchBar = $('#search')
    const loginForm = $('#loginForm')
    const contactPageClass = $('.contactPage')
    const landingPageClass = $('.landingPage')
    const headerOfInterest = $('#ofInterest')
    const userLoggedIn = $('#userLoggedIn')
    const aboutUsPageClass = $('.aboutUsPage')
    const clockPageClass = $('.clockPage')
    const checkboxAlert = $("#checkboxAlert")
    const contactForm = $("#contactForm")

    $('.imgGallery').bxSlider({
        mode: 'fade',
        captions: false,
        slideWidth: 1200,
        Responsive: true,
        pager: true
    });

    //Loading and validating current theme from localStorage and HTML page:
    var hrefTheme = $('.theme');
    var localStorageTheme = localStorage.getItem("themeKey");
    var loadedTheme = hrefTheme.attr('href');
    var isEmptyLocalStorage = loadedTheme == localStorageTheme;

    if (isEmptyLocalStorage) {
        hrefTheme.attr('href', 'css/green.css');
        localStorage.setItem("themeKey", 'css/green.css');
    } else {
        hrefTheme.attr('href', localStorageTheme);
    }

    //Toggle the themes in the page  
    $('#to-green').click(function() {
        hrefTheme.attr('href', 'css/green.css');
        localStorage.setItem("themeKey", 'css/green.css');
    });

    $('#to-blue').click(function() {
        hrefTheme.attr('href', 'css/blue.css');
        localStorage.setItem("themeKey", 'css/blue.css');
    });

    $('#to-red').click(function() {
        hrefTheme.attr('href', 'css/red.css');
        localStorage.setItem("themeKey", 'css/red.css');
    });

    // postsArray is defined in jsonPosts.js loaded also in the HTML:
    function getPostsArray(text, entry) {
        let lengthArray = entry === "All" ? postsArray.length : entry

        return postsArray.slice(0, lengthArray).map(element => {
                element.title = element.title.toLowerCase()
                element.content = element.content.toLowerCase()
                return element
            })
            .filter(element => element.title.includes(text) || element.content.includes(text))
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function populateArticles(text = "", entry = 4) {
        let postsArray = getPostsArray(text, entry)
        let postTemplate

        if (postsArray.length > 0) {
            postsArray.map(element => {
                    element.title = capitalizeFirstLetter(element.title)
                    element.content = capitalizeFirstLetter(element.content)
                    return element
                })
                .forEach(item => {
                    postTemplate = `
                    <article class="post">
                    <h2> ${item.title} </h2>
                    <span class="date"> ${item.date}  </span>
                    <p>
                    ${item.content} 
                    </p>
                    <a href="#" class="button-more"> Read more </a>
                    </article>`;

                    $('#posts').append(postTemplate);
                });
        } else {
            postTemplate = `
            <article class="post">        
            <p id="notFoundLbl" class="errorAlert">
            Sorry, search results not found!
            </p>       
            </article>`;

            $('#posts').append(postTemplate);
        }
    }

    function cleanArticles() {
        $('.post').remove();
    }

    populateArticles()

    function updateEntries(dropEntries) {
        dropEntries = dropEntries ? dropEntries : $('.post').length
        $('#entryDrop').val(dropEntries)
    }

    function getCurrentArticlesPost() {
        const articles = Array.from($('.post'))
        return articles.map(el => { return el.textContent })
    }

    // Select (DDL) component:
    $('#entryDrop').change(function() {
        let entry = $(this).val();
        let postsArrayPre = getCurrentArticlesPost()

        cleanArticles()
        populateArticles("", entry)
        let postsArrayPro = getCurrentArticlesPost()
        const containsAll = postsArrayPre.every(result => postsArrayPro.includes(result))

        if (!containsAll) {
            searchBar.val('')
            alert('The change in your entries has probably changed your search results')
        }
    });

    searchBar.bind("keyup", function(value) {
        var text = $(this).val().toLowerCase().trim();
        let keyPressed = value.originalEvent.code

        if (keyPressed === "Enter" || keyPressed === "NumpadEnter") {
            cleanArticles()
            populateArticles(text)
            updateEntries()
        }

        if (keyPressed === "Escape") {
            searchBar.val('')
            cleanArticles()
            populateArticles()
            updateEntries()
        }
    });

    function setLogin() {
        var savedCredential = localStorage.getItem('credentials');
        var isThereEmail = savedCredential != null

        if (isThereEmail) {
            loginForm.hide();
            userLoggedIn.show()
                .html(savedCredential.substring(0, savedCredential.indexOf('+')));
            $('#logout').show();
        }
    }

    setLogin()

    function assertLogin() {
        var form_name = $('#name').val().trim();
        var form_email = $('#email').val().trim();
        var form_password = $('#password').val().trim();

        var credentials = `${form_name}+${form_email}${form_password}`;
        localStorage.setItem('credentials', credentials);
        alert('User registered successfully!\nYou have been automatically logged in');
        setLogin()
    }

    loginForm.validate({
        submitHandler: function() {
            assertLogin()
        }
    });

    $('#logout').click(function() {
        localStorage.removeItem("credentials");
        userLoggedIn.hide()
        location.reload();
    });

    //Link to back to the top (go up):
    $('.goUp').click(function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });

    function createWebElement(element) {
        return document.createElement(element);
    }

    // To be used toggling from "landingPage" to another views:
    function toggleFromLandingPage() {
        if ($('#sidebar br').length != 1) {
            var extraSpace = createWebElement("br");
            headerOfInterest.prepend(extraSpace);
            headerOfInterest.css('margin-top', '-=-20px');
        }
        landingPageClass.hide();
    }

    // landingPage
    $('#landingPage').click(function() {
        if ($('#sidebar br').length) {
            $('#sidebar br').remove();
            headerOfInterest.css('margin-top', '');
        }

        landingPageClass.show();
        aboutUsPageClass.hide();
        contactPageClass.hide();
        clockPageClass.hide();
    });

    // About us page
    $('#aboutUsLnk').click(function() {
        toggleFromLandingPage();
        $('#accordion').accordion({
            heightStyle: "content",
            autoHeight: false,
            clearStyle: true,
        });

        aboutUsPageClass.show();
        contactPageClass.hide();
        clockPageClass.hide();
    });

    // Table and Clock page
    $('#clockLnk').click(function() {
        toggleFromLandingPage();
        setInterval(function() {
            var clock = moment().format("hh:mm:ss");
            $('#clockContent').html(clock);
        }, 1000);

        clockPageClass.show();
        aboutUsPageClass.hide();
        contactPageClass.hide();
    });

    // Contact page
    $('#contactLnk').click(function() {
        toggleFromLandingPage();

        contactPageClass.show();
        aboutUsPageClass.hide();
        clockPageClass.hide();
    });

    contactForm.validate({
        rules: {
            checkbox: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            var webElement = element.attr("name")
            error.insertAfter(element);

            if (webElement === 'checkbox')
                error.insertAfter(checkboxAlert);
        },
        submitHandler: function(form) {
            alert("Your message was sent successfully!\nWe'll be replying you as soon as possible")
            form.submit()
        }
    });

});